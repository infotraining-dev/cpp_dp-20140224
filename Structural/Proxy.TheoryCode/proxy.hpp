#ifndef PROXY_HPP_
#define PROXY_HPP_

#include <iostream>
#include <string>

// "Subject" 
class Subject
{
public:
	virtual void request() = 0;
	virtual ~Subject() {}
};

// "RealSubject"
class RealSubject : public Subject
{
public:
	RealSubject()
	{
		std::cout << "RealSubject's creation" << std::endl;
	}

	~RealSubject()
	{
		std::cout << "RealSubject's clean-up" << std::endl;
	}
	
	void request()
	{
		std::cout << "Called RealSubject.request()" << std::endl;
	}
};

// "Proxy" 
class Proxy : public Subject
{
	RealSubject* real_subject_;
	
	Proxy(const Proxy&);
	Proxy& operator=(const Proxy&);
public:
	Proxy() :
		real_subject_(0)
	{
		std::cout << "Proxy's creation" << std::endl;
	}

	~Proxy()
	{
		delete real_subject_;
	}

	void request()
	{
		// lazy initialization'
		if (real_subject_ == NULL)
		{
			real_subject_ = new RealSubject();
		}

		real_subject_->request();
	}
};

#endif /*PROXY_HPP_*/
