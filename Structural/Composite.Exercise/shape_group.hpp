#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <list>
#include <algorithm>


namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupujący kształty geometryczne
class ShapeGroup : public Shape
{
    typedef boost::shared_ptr<Shape> ShapePtrType;
    std::list<ShapePtrType> shapes_;
public:
    ShapeGroup ()
    {}

    ShapeGroup(const ShapeGroup& source)
    {
        for(auto shape : source.shapes_)
        {
            shapes_.push_back(ShapePtrType(shape->clone()));
        }
    }

    void add(ShapePtrType shape)
    {
        shapes_.push_back(shape);
    }

    void draw() const
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::draw, _1));
    }

    void move(int dx, int dy)
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::move, _1, dx, dy));
    }

    ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream &in)
    {
        int count;
        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string id;
            in >> id;

            ShapePtrType shape(ShapeFactory::instance().create(id));
            shape->read(in);
            add(shape);
        }
    }

    void write(std::ostream &out)
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::write, _1, boost::ref(out)));
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
