#include <iostream>
#include <vector>
#include <boost/flyweight.hpp>

using namespace std;

class Platnik
{
    int id_;
    boost::flyweight<string> imie_;
    boost::flyweight<string> nazwisko_;
public:
    Platnik(int id, const string& imie, const string& nazwisko)
        : id_(id), imie_(imie), nazwisko_(nazwisko)
    {}

    int id() const
    {
        return id_;
    }

    string imie() const
    {
        return imie_;
    }

    void set_imie(const string& imie)
    {
        imie_ = imie;
    }

    string nazwisko() const
    {
        return nazwisko_;
    }

    void set_nazwisko(const string& nazwisko)
    {
        nazwisko_ = nazwisko;
    }

    bool operator==(const Platnik& p)
    {
        return id_ == p.id_;
    }
};


int main()
{
    vector<Platnik> platnicy;
    platnicy.push_back(Platnik(1, "Jan", "Kowalski"));
    platnicy.push_back(Platnik(2, "Jan", "Nowak"));
    platnicy.push_back(Platnik(3, "Anna", "Kowalka"));
    platnicy.push_back(Platnik(1, "Anna", "Nowakowska"));


    for(auto& p : platnicy)
    {
        cout << p.imie() << " " << p.nazwisko() << endl;
    }

    return 0;
}

