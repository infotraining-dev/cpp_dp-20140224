#include "paragraph.hpp"

#include <iostream>

using namespace std;

int main()
{
	// normal paragraph
//	Paragraph* p = new Paragraph("Test of paragraph");
//	cout << "Normal paragraph: " << p->getHTML() << endl;
//	delete p;

//	// bolded paragraph
//	Paragraph* bold = new BoldParagraph(new Paragraph("Test of bold paragraph"));
//	cout << "Bolded paragraph: " << bold->getHTML() << endl;
//	delete bold;

//	// bold and italic
//	Paragraph* bold_and_italic = new ItalicParagraph(new BoldParagraph(new Paragraph("Test of italic and bold paragraph")));
//	cout << "Bold and italic: " << bold_and_italic->getHTML() << endl;
//	delete bold_and_italic;

    Paragraph p("text");
    BoldParagraph bp1(p);
    BoldParagraph bp2(bp1);
    ItalicParagraph ip(bp2);

    cout << ip.getHTML() << endl;

}
