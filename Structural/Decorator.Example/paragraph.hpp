#ifndef PARAGRAPH_H_
#define PARAGRAPH_H_

#include <iostream>
#include <string>

class Paragraph
{
protected:
	std::string text_;
public:
	Paragraph(const std::string& initial_text) : text_(initial_text)
	{
	}
	
	virtual ~Paragraph() {}
	
	virtual std::string getHTML() const
	{
		return text_;
	}
};

class BoldParagraph : public Paragraph
{
protected:
    const Paragraph& wrapped_paragraph_;
public:
    BoldParagraph(const Paragraph& paragraph) : Paragraph(""), wrapped_paragraph_(paragraph)
	{
	}

    BoldParagraph(const BoldParagraph& paragraph) : Paragraph(""), wrapped_paragraph_(paragraph)
    {
    }

//	~BoldParagraph()
//	{
//		delete wrapped_paragraph_;
//	}

	virtual std::string getHTML() const
	{
        return "<b>" + wrapped_paragraph_.getHTML() + "</b>";
	}
};

class ItalicParagraph : public Paragraph
{
protected:
    const Paragraph& wrapped_paragraph_;
public:
    ItalicParagraph(const Paragraph& paragraph) : Paragraph(""), wrapped_paragraph_(paragraph)
	{
	}

//	~ItalicParagraph()
//	{
//		delete wrapped_paragraph_;
//	}

    virtual std::string getHTML() const
	{
        return "<i>" + wrapped_paragraph_.getHTML() + "</i>";
	}
};

#endif /*PARAGRAPH_H_*/
