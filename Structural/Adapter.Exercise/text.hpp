#ifndef TEXT_HPP
#define TEXT_HPP

#include <string>
#include "shape.hpp"
#include "paragraph.hpp"

namespace Drawing
{

//TODO - zaadaptować klase Paragraph do wymogow klienta
class Text : public ShapeBase, private LegacyCode::Paragraph
{
public:
    Text(int x = 0, int y = 0, const std::string& txt = "")
        : ShapeBase(x, y), LegacyCode::Paragraph(txt.c_str())
    {
    }

    void draw() const
    {
        render_at(point().x(), point().y());
    }

    Text* clone() const
    {
        return new Text(*this);
    }

    void read(std::istream &in)
    {
        Point pt;
        std::string text;

        in >> pt >> text;

        set_point(pt);
        set_paragraph(text.c_str());
    }

    void write(std::ostream &out)
    {
        out << "Text " << point() << " " << get_paragraph() << std::endl;
    }
};

}

#endif
