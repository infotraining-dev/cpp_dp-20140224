#ifndef GAME_HPP_
#define GAME_HPP_

#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

#include "enemy_factory.hpp"

namespace Game {

enum GameLevel { EASY, DIE_HARD };

class GameApp
{
	std::vector<Enemy*> enemies_;
	AbstractEnemyFactory* enemy_factory_;

	GameApp(const GameApp&);
	GameApp& operator=(const GameApp&);
public:
	GameApp() : enemy_factory_(NULL)
	{
	}

	void select_level(GameLevel level)
	{
		if (enemy_factory_)
			delete enemy_factory_;

		switch (level)
		{
			case EASY:
				enemy_factory_ = new EasyLevelEnemyFactory();
				break;
			case DIE_HARD:
				enemy_factory_ = new DieHardLevelEnemyFactory();
				break;
		}
	}

	void init_game(int number_of_enemies)
	{
		for(int i = 0; i <  number_of_enemies/3; ++i)
		{
			enemies_.push_back(enemy_factory_->CreateSoldier());
			enemies_.push_back(enemy_factory_->CreateMonster());
			enemies_.push_back(enemy_factory_->CreateSuperMonster());
		}
	}

	void test_monsters()
	{
		for_each(enemies_.begin(), enemies_.end(), std::mem_fun(&Enemy::action));
	}

	~GameApp()
	{
		for(size_t i = 0; i < enemies_.size(); ++i)
			delete enemies_[i];

		delete enemy_factory_;
	}
};

}
#endif /*GAME_HPP_*/
