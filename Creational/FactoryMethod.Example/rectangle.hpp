#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <iostream>
#include "shape.hpp"

namespace Drawing
{

class Rectangle: public ShapeBase
{
	int width_;
	int height_;
public:
	Rectangle(int x = 0, int y = 0, int width = 0, int height = 0) :
			ShapeBase(x, y), width_(width), height_(height)
	{
	}

	int width() const
	{
		return width_;
	}

	void set_width(int width)
	{
		width_ = width;
	}

	int height() const
	{
		return height_;
	}

	void set_height(int height)
	{
		height_ = height;
	}

	virtual void draw() const
	{
		std::cout << "Drawing a rectangle at: " << point() << " width: "
				<< width_ << " height: " << height_ << std::endl;
	}

	virtual void read(std::istream& in)
	{
		Point pt;

		in >> pt >> width_ >> height_;

		set_point(pt);
	}

	virtual void write(std::ostream& out)
	{
		out << "Rectangle " << point() << " " << width_ << " " << height_
				<< std::endl;
	}
};

class RectangleCreator : public ShapeCreator
{
public:
    Rectangle* create()
    {
        return new Rectangle();
    }
};

}

#endif
