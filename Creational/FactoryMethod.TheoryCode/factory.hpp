#ifndef FACTORY_HPP_
#define FACTORY_HPP_

#include <iostream>
#include <string>

// "Product"
class Product
{
public:
	// interfejs produktu
	virtual std::string description() const = 0;
	virtual ~Product() {}
};

// "ConcreteProductA"
class ConcreteProductA : public Product
{
public:
	std::string description() const
	{
		return std::string("ConcreteProductA");
	}
};

// "ConcreteProductB"
class ConcreteProductB : public Product
{
public:
	std::string description() const
	{
		return std::string("ConcreteProductB");
	}
};

// "Creator"
class Creator
{
public:
	virtual Product* factory_method() = 0;
	virtual ~Creator() {}
};

// "ConcreteCreator"
class ConcreteCreatorA : public Creator
{
public:
	virtual Product* factory_method()
	{
		return new ConcreteProductA();
	}
};

// "ConcreteCreator"
class ConcreteCreatorB : public Creator
{
public:
	Product* factory_method()
	{
		return new ConcreteProductB();
	}
};

#endif /*FACTORY_HPP_*/
