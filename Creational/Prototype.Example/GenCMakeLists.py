import glob

cmake_content = """get_filename_component(ProjectId ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})
cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)
add_executable(${PROJECT_NAME} ${SRC_LIST})
ADD_DEFINITIONS("-std=c++0x -fpermissive -Ofast")
target_link_libraries( ${PROJECT_NAME} stdc++ pthread boost_thread boost_system)"""

header_files = glob.glob("*.hpp")

cmake_content_with_headers = cmake_content.replace("${SRC_LIST}", "${SRC_LIST} " + " ".join(header_files))

with open("CmakeLists.txt", "w") as cmake_file:
	cmake_file.write(cmake_content_with_headers)