/*
 * square.hpp
 *
 *  Created on: 04-02-2013
 *      Author: Krystian
 */

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

// TODO: Doda� klase Square
namespace Drawing
{
    class Square : public ShapeBase
    {
        int size_;
    public:
        Square(int x = 0, int y = 0, int size = 0) : ShapeBase(x, y), size_(size)
        {
        }

        void draw() const
        {
            std::cout << "Drawing square at " << point() << " size: " << size_ << std::endl;
        }

        int size() const
        {
            return size_;
        }

        void set_size(int size)
        {
            size_ = size;
        }

        virtual void read(std::istream& in)
        {
            Point pt;
            unsigned int size;

            in >> pt >> size;

            set_point(pt);
            set_size(size);
        }

        virtual void write(std::ostream& out)
        {
            out << "Square " << point() << " " << size() << std::endl;
        }
    };
}


#endif /* SQUARE_HPP_ */
