#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include <iostream>
#include "shape.hpp"

namespace Drawing
{

class Circle : public ShapeBase
{
private:
	unsigned int radius_;
public:
	Circle(int x = 0, int y = 0, size_t radius = 0) : ShapeBase(x, y), radius_(radius)
	{
	}

	unsigned int radius() const
	{
		return radius_;
	}

	void set_radius(unsigned int radius)
	{
		radius_ = radius;
	}

	virtual void draw() const
	{
		std::cout << "Drawing a circle at: " << point() << " radius: " << radius_ << std::endl;
	}

	virtual void read(std::istream& in)
	{
		Point pt;
		unsigned int radius;

		in >> pt >> radius;

		set_point(pt);
		set_radius(radius);
	}

	virtual void write(std::ostream& out)
	{
		out << "Circle " << point() << " " << radius() << std::endl;
	}
};

}

#endif
