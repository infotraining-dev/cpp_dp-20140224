#ifndef SIMPLE_SINGLETON_HPP_
#define SIMPLE_SINGLETON_HPP_

#include <iostream>

namespace Simple
{

// leniwa wersja z operatorem new ---------------------------------------------
template <typename T>
class SingletonHolder
{
public:
	static T& instance()
	{
        if (!instance_)
           instance_ = new T();

		return *instance_;
	}

private:
	SingletonHolder();
	SingletonHolder(const SingletonHolder&);
	SingletonHolder& operator=(const SingletonHolder&);

	static T* instance_;
};

template <typename T>
T* SingletonHolder<T>::instance_ = 0;

}
#endif /* SIMPLE_SINGLETON_HPP_ */
