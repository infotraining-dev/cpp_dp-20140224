#include "clone_factory.hpp"
#include "image.hpp"

namespace
{
    using namespace Drawing;

    bool is_drawing = ShapeFactory::instance().register_shape("Image", new Image());
}
