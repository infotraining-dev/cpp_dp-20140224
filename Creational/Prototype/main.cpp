#include <iostream>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual Engine* clone() const = 0;
    virtual ~Engine() {}
};

class Diesel : public Engine
{
public:
    virtual void start()
    {
        cout << "Diesel starts\n";
    }

    virtual void stop()
    {
        cout << "Diesel stops\n";
    }

    Diesel* clone() const
    {
        return new Diesel(*this);
    }
};

class TDI : public Diesel
{
public:
    virtual void start()
    {
        cout << "TDI starts\n";
    }

    virtual void stop()
    {
        cout << "TDI stops\n";
    }
};

class Hybrid : public Engine
{
public:
    virtual void start()
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop()
    {
        cout << "Hybrid stops\n";
    }

    virtual Hybrid* clone() const
    {
        return new Hybrid(*this);
    }
};

class Car
{
    Engine* engine_;
public:
    Car(Engine* engine) : engine_(engine)
    {}

    Car(const Car& source) : engine_(source.engine_->clone())
    {
    }

    ~Car()
    {
        delete engine_;
    }

    void drive(int km)
    {
        engine_->start();
        cout << "Drive: " << km << " km\n";
        engine_->stop();
    }
};

int main()
{
    Car c1(new TDI());

    c1.drive(100);

    cout << "\nCopy of c1:\n";

    Car copy_of_c1 = c1;

    copy_of_c1.drive(400);

    cout << "\n";

    Car c2(new Hybrid());

    c2.drive(200);

    cout << "\nCopy of c2:\n";

    Car copy_of_c2 = c2;

    copy_of_c2.drive(500);
}

