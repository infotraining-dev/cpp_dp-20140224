#include "document.hpp"
#include "command.hpp"

std::string Clipboard::content = "Tekst ze schowka...";

Document::~Document()
{
	// czyszczenie stosu
	while (!commands_.empty())
	{
		Command* cmd = commands_.top();
		delete cmd;
		commands_.pop();
	}
}

void Document::set_memento(Document::Memento& memento)
{
	text_ = memento.snapshot_;
}

