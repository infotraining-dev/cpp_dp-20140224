#include <iostream>

class TurnstileFSM;
class LockedState;
class UnlockedState;

class TurnstileState
{
public:
	virtual TurnstileState* Coin(TurnstileFSM*) = 0;
	virtual TurnstileState* Pass(TurnstileFSM*) = 0;
	virtual ~TurnstileState() {}
};

class UnlockedState : public TurnstileState
{
public:
	TurnstileState* Coin(TurnstileFSM* t);
	TurnstileState* Pass(TurnstileFSM* t);
};

class LockedState : public TurnstileState
{
public:
	TurnstileState* Coin(TurnstileFSM* t);
	TurnstileState* Pass(TurnstileFSM* t);
};

class Turnstile
{
public:
	virtual void Lock() { std::cout << "Lock" << std::endl; }
	virtual void Unlock() { std::cout << "Unlock" << std::endl; };
	virtual void Thankyou() { std::cout << "Thank You" << std::endl; };
	virtual void Alarm() { std::cout << "Alarm" << std::endl; };
	virtual ~Turnstile() {}
};

class TurnstileFSM : public Turnstile
{
public:
	TurnstileFSM() : itsState(&lockedState)
	{
	}

	void Coin() { itsState = itsState->Coin(this); }
	void Pass() { itsState = itsState->Pass(this); }
	
	static LockedState lockedState;
	static UnlockedState unlockedState;

private:
	TurnstileState* itsState;
};

LockedState TurnstileFSM::lockedState;
UnlockedState TurnstileFSM::unlockedState;

TurnstileState* UnlockedState::Coin(TurnstileFSM* t)
{
	t->Thankyou();
	return this;
}

TurnstileState* UnlockedState::Pass(TurnstileFSM* t)
{
	t->Lock();
	return &TurnstileFSM::lockedState;
}

TurnstileState* LockedState::Coin(TurnstileFSM* t)
{
	t->Unlock();
	return &TurnstileFSM::unlockedState;
}

TurnstileState* LockedState::Pass(TurnstileFSM* t)
{
	t->Alarm();
	return this;
}

int main()
{
	TurnstileFSM turnstile;

	turnstile.Coin();
	turnstile.Pass();
	turnstile.Pass();
	turnstile.Coin();
	turnstile.Coin();
}
